import pandas as pd

import matplotlib.pyplot as plt

# import os

# dirs = os.listdir("C:/Users/kylma/python-workspace/data-science/ml-latest-small/")

# print(dirs)

movies = pd.read_csv("C:/Users/kylma/python-workspace/data-science/ml-latest-small/movies.csv")
avaliacoes = pd.read_csv("C:/Users/kylma/python-workspace/data-science/ml-latest-small/ratings.csv")

movies.columns = ['filmeId','titulo','generos']
avaliacoes.columns = ['usuarioId','filmeId','nota','momento']

notas_medias_por_filme = avaliacoes.groupby("filmeId")["nota"].mean()

filmes_com_media = movies.join(notas_medias_por_filme, on="filmeId")

filmes_com_media_ordenados_por_media_maior_nota = filmes_com_media.sort_values("nota", ascending=False)


print(avaliacoes.query("filmeId == 1")["nota"].plot(kind='hist'))
